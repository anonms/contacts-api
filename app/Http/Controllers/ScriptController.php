<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;
use App\Fullcontact;
use App\Clearbit;
use DB;

class ScriptController extends Controller {

	public function index(Clearbit $getClearbit, Fullcontact $getFullcontact, Request $request)
	{
		$email = $request->email;
    	$getClearbit = DB::table('clearbit_contacts')->where('email', $email)->first();
    	$getFullcontact = DB::table('fullcontact_contacts')->where('email', $email)->first();
		
		if (isset($getClearbit) && isset($getFullcontact)){
			return view('seller', compact('getClearbit', 'getFullcontact'));
		}

		elseif(!isset($getClearbit) || !isset($getFullcontact)){
			if(!isset($getClearbit)){
				$remoteClearbit = $this->getDataFromRemote('Clearbit', $email);
				if(isset($remoteClearbit)) {
					if($remoteClearbit->getStatusCode() == 200) {
						$this->initializeData('Clearbit', $remoteClearbit, $email);
						$getClearbit = DB::table('clearbit_contacts')->where('email', $email)->first();
						return view('seller', compact('getClearbit', 'getFullcontact'));
					}
					else {
						$message = $remoteFullcontact->getBody();
						return view('seller', compact('message'));
					}	
				}	
			}
			$remoteFullcontact = $this->getDataFromRemote('Fullcontact', $email);
			if(isset($remoteFullcontact)){
				if($remoteFullcontact->getStatusCode() == 200){
					$this->initializeData('Fullcontact', $remoteFullcontact, $email);
					$getFullcontact = DB::table('fullcontact_contacts')->where('email', $email)->first();
					return view('seller', compact('getClearbit', 'getFullcontact'));
				}
				else {
					$message = $remoteFullcontact->getBody();
					return view('seller', compact('message'));
				}	
				
			}
		}
		elseif(!isset($getClearbit) && isset($getFullcontact)){
			$remoteClearbit = $this->getDataFromRemote('Clearbit', $email);
			if(isset($remoteClearbit)) {
				if($remoteClearbit->getStatusCode() == 200) {
					$this->initializeData('Clearbit', $remoteClearbit, $email);
					$getClearbit = DB::table('clearbit_contacts')->where('email', $email)->first();
					return view('seller', compact('getClearbit', 'getFullcontact'));
				}
				else {
					$message = $remoteFullcontact->getBody();
					return view('seller', compact('message'));
				}	
			}
		}
		else {
			$remoteClearbit = $this->getDataFromRemote('Clearbit', $email);
			$remoteFullcontact = $this->getDataFromRemote('Fullcontact', $email);
			if(isset($remoteClearbit) && isset($remoteFullcontact)){
				if($remoteClearbit->getStatusCode() == 200 && $remoteFullcontact->getStatusCode() == 200){
					$this->initializeData('Clearbit', $remoteClearbit, $email);
					$this->initializeData('Fullcontact', $remoteFullcontact, $email);
					$getClearbit = DB::table('clearbit_contacts')->where('email', $email)->first();
    				$getFullcontact = DB::table('fullcontact_contacts')->where('email', $email)->first();
    				return view('seller', compact('getClearbit', 'getFullcontact'));
				}
				else{
					if($remoteClearbit->getStatusCode() != 200){
						$this->initializeData('Clearbit', $remoteClearbit, $email);
					}
					else {
						$this->initializeData('Fullcontact', $remoteFullcontact, $email);
					}
					$message = 'Sorry but you cannot be registered';
					return view('seller', compact('message'));		
				}
			}
		}
	}

	public function getDataFromRemote($site, $email)
	{	
		if($site == 'Clearbit'){
			$clearbitUrl = new Client([
				'base_uri' => 'http://person.clearbit.com/v1/', 
				'verify' => false,
				'exceptions' => false
				]);
			$request = $clearbitUrl->get('people/email/'.$email, array('auth' => array('161bf098965f4a958306537149a6aa49', '')));
			
	    }
		else {
			$fullcontactUrl = new Client([
				'base_uri' => 'https://api.fullcontact.com/v2/', 
				'verify' => false,
				'exceptions' => false
				]);
	 		$request = $fullcontactUrl->get('person.json?email='.$email.'&apiKey=d3c80cf56de1b44e');
	 		
		}
		$this->storeResponse($email, $request->getBody(), $site);
		return $request;
		
	}

	public function initializeData($site, $response, $email)
	{
		$array = json_decode($response->getBody(), true);
		
		if($site == 'Clearbit'){
			$firstname = $this->ifInfoExists('name.givenName', $array);
	    	$lastname = $this->ifInfoExists('name.familyName', $array);
	    	$photo = $this->ifInfoExists('avatar', $array);
	    	$company = $this->ifInfoExists('employment.name', $array);
	    	$facebook = $this->ifInfoExists('facebook.handle', $array);
		}
		else {
			$firstname = $this->ifInfoExists('contactInfo.givenName', $array);
		    $lastname = $this->ifInfoExists('contactInfo.familyName', $array);
		    $photo = $this->ifInfoExists('photos.0.url', $array);
		    $company = $this->ifInfoExists('organizations.0.name', $array);
		    $socialProfiles = $this->ifFullcontactSocialProfileExists($array);
		    $facebook = $this->ifFullcontactFbExsists($socialProfiles);
		}
		$this->storeToLocal($site, $firstname, $lastname, $photo, $email, $company, $facebook);
	}

	public function storeToLocal($site, $firstname, $lastname, $photo, $email, $company, $facebook){
		if($site == 'Clearbit'){
			DB::insert('insert into clearbit_contacts (firstname, lastname, photo, email, company, facebook) values (?,?,?,?,?,?)', [$firstname , $lastname, $photo, $email, $company, $facebook]);
		}
		else{
			DB::insert('insert into fullcontact_contacts (firstname, lastname, photo, email, company, facebook) values (?,?,?,?,?,?)', [$firstname , $lastname, $photo, $email, $company, $facebook]);
		}
	}

	public function storeResponse($email, $response, $site)
	{
		DB::insert('insert into responses (email, message, site) values (?,?,?)', [$email, $response, $site]);
	}

	public function ifInfoExists($key, $array)
	{
		if(array_has($array, $key)){
			return array_get($array, $key);
		}
		else{
			return NULL;
		}
	}	

	public function ifFullcontactSocialProfileExists($info)
 	{
 		if (!isset($info['socialProfiles'])) {
 			return array('SocialProfiles' => array('url' => 'No social profiles'));
 		}
 		return $info['socialProfiles'];
 	}

 	public function ifFullcontactFbExsists($socialProfiles)
 	{
 		foreach ($socialProfiles as $socialProfile) {
   			foreach ($socialProfile as $index => $url) {
   				if($index == 'url'){
   					$string = "/(facebook)/";
					if (preg_match_all($string, $url)) {
						return $url;
					}
					return NULL;
				}
    		}
    	}
    }	
}


