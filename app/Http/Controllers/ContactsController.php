<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests;
use App\Contact;
use DB;

class ContactsController extends Controller
{
   
    /**
     * Display the specified resource.
     *
     * @param  string $email
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        $clearbitSite = 'clearbit';
        $fullcontactSite = 'fullcontact';
        $clearbit = $this->query($email, $clearbitSite);
        $fullcontact = $this->query($email, $fullcontactSite);

        // If contact is not in local Clearbit
        if(!isset($clearbit) && isset($fullcontact)) {
            $clearbit = $this->getDataFromRemote($clearbitSite, $email);
            if(isset($clearbit)) {
                if($clearbit->getStatusCode() == 200) {
                    $this->initializeData($clearbitSite, $clearbit, $email); 
                    $clearbit = Contact::where(['email' => $email, 'site' => $clearbitSite])->first();

                    return response()->json([
                        'status' => 200,
                        'contact' => $this->transform($clearbit->toArray())
                    ], 200); 
                }
                elseif($clearbit->getStatusCode() != 200) {
                   return response()->json([
                        'status' => 200,
                        'contact' => $this->transform($fullcontact->toArray())
                    ], 200); 
                }
            }
            else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Internal server error'
                ], 200); 
            }
        }
        // If contact is not in local Fullcontact
        elseif(!isset($fullcontact) && isset($clearbit)) {
            $fullcontact = $this->getDataFromRemote($fullcontactSite, $email);
            if(isset($fullcontact)) {
                if($fullcontact->getStatusCode() == 200) {
                    $this->initializeData($fullcontactSite, $fullcontact, $email);
                    $fullcontact = Contact::where(['email' => $email, 'site' => $fullcontactSite])->first();
                    
                    return response()->json([
                        'status' => 200,
                        'contact' => $this->transform($fullcontact->toArray())
                    ], 200);  
                }
                elseif($fullcontact->getStatusCode() != 404) {
                    return response()->json([
                        'status' => 200,
                        'contact' => $this->transform($clearbit->toArray())
                    ], 200);  
                }
            }
            else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Internal server error'
                ], 200); 

            }
        }
        // If both records don't exist on local DB
        elseif(!isset($clearbit) && !isset($fullcontact)) {
            $clearbit = $this->getDataFromRemote('clearbit', $email);
            $fullcontact = $this->getDataFromRemote('fullcontact', $email);
            if(isset($clearbit) && isset($fullcontact)) {
                // If Clearbit has result && Fullcontact doesn't
                if($clearbit->getStatusCode() == 200 && $fullcontact->getStatusCode() != 200) {
                    $this->initializeData($clearbitSite, $clearbit, $email);
                    $clearbit = Contact::where(['email' => $email, 'site' => $clearbitSite])->first();
                    
                    return response()->json([
                        'status' => 200,
                        'contact' =>  $this->transform($clearbit->toArray())
                    ], 200);   
                } 
                // If Fullcontact has result && Clearbit doesn't
                elseif($fullcontact->getStatusCode() == 200 && $clearbit->getStatusCode() != 200) {
                    $this->initializeData($fullcontactSite, $fullcontact, $email);
                    $fullcontact = Contact::where(['email' => $email, 'site' => $fullcontactSite])->first();
                    
                    return response()->json([
                        'status' => 200,
                        'contact' =>  $this->transform($fullcontact->toArray())
                    ], 200);   
                }
                // If both don't have results
                elseif($clearbit->getStatusCode() != 200 && $fullcontact->getStatusCode() != 200) {
                    return response()->json([
                        'status' => 404,
                        'message' => 'Contact not found'
                    ], 200);
                }
                // If both have results
                else {
                    $this->initializeData('clearbit', $clearbit, $email);
                    $this->initializeData('fullcontact', $fullcontact, $email);
                    $clearbit = Contact::where(['email' => $email, 'site' => $clearbitSite])->first();
                    $fullcontact = Contact::where(['email' => $email, 'site' => $fullcontactSite])->first();
                    
                    return response()->json([
                        'status' => 200,
                        'contact' =>  $this->transform($clearbit->toArray())
                    ], 200);   
                    
                }
            }
            else {
                return response()->json([
                    'status' => 500,
                    'message' =>  'Internal server error'
                ], 200);
            }
            
        }
        // If both records exist in local
        else {
            return response()->json([
                'status' => 200,
                'contact' =>  $this->transform($clearbit->toArray())
            ], 200);
        }
    }


    /**
     * Custom Functions Section
     *
     * 
     */
    
    /**
     * Queries the contacts table
     * @param  string $email 
     * @param  string $site  
     * @return string
     */
    public function query($email, $site)
    {
        $query = Contact::where(['email' => $email, 'site' => $site])->first();
        return $query;
    }
    /**
     * Transforms a group of objects to php array
     * @param  array $contacts
     * @return array
     */
    private function transformCollection($contacts)
    {
        return array_map([$this, 'transform'], $contacts->toArray());
    }
    
    /**
     * Transforms single object to php array
     * @param  object $contact 
     * @return array
     */
    public function transform($contact)
    {
        return [
            'firstname' => $contact['firstname'],
            'lastname' => $contact['lastname'],
            'email' => $contact['email'],
            'company' => $contact['company'],
            'facebook' => $contact['facebook'],
            'avatar' => $contact['photo']
        ];
   }
    
    /**
     * Search email using Clearbit and Fullcontact APIs
     * @param  string $site  
     * @param  string $email 
     * @return array        
     */
    public function getDataFromRemote($site, $email)
    {   
        if($site == 'clearbit'){
            $clearbitUrl = new Client([
                'base_uri' => 'http://person.clearbit.com/v1/', 
                'verify' => false,
                'exceptions' => false
                ]);
            $request = $clearbitUrl->get('people/email/'.$email, array('auth' => array('161bf098965f4a958306537149a6aa49', '')));
            
        }
        else {
            $fullcontactUrl = new Client([
                'base_uri' => 'https://api.fullcontact.com/v2/', 
                'verify' => false,
                'exceptions' => false
                ]);
            $request = $fullcontactUrl->get('person.json?email='.$email.'&apiKey=392c55b70f7de61b');
        }

        $this->storeResponse($email, $request->getBody(), $site);
        return $request;
    }
    /**
     * Stores all responses from Clearbit and Fullcontact API requests
     * @param  string $email    
     * @param  array $response 
     * @param  string $site     
     */
    public function storeResponse($email, $response, $site)
    {
        DB::insert('insert into responses (email, message, site) values (?,?,?)', [$email, $response, $site]);
    }
    /**
     * Gets the data from response and assign them in variables
     * @param  string $site     
     * @param  array $response 
     * @param  string $email    
     */
    public function initializeData($site, $response, $email)
    {
        $array = json_decode($response->getBody(), true);
        
        if($site == 'clearbit'){
            $firstname = $this->ifInfoExists('name.givenName', $array);
            $lastname = $this->ifInfoExists('name.familyName', $array);
            $photo = $this->ifInfoExists('avatar', $array);
            $company = $this->ifInfoExists('employment.name', $array);
            $facebook = $this->ifInfoExists('facebook.handle', $array);
        }
        else {
            $firstname = $this->ifInfoExists('contactInfo.givenName', $array);
            $lastname = $this->ifInfoExists('contactInfo.familyName', $array);
            $photo = $this->ifInfoExists('photos.0.url', $array);
            $company = $this->ifInfoExists('organizations.0.name', $array);
            $socialProfiles = $this->ifFullcontactSocialProfileExists($array);
            $facebook = $this->ifFullcontactFbExsists($socialProfiles);
        }
        $this->storeToLocal($site, $firstname, $lastname, $photo, $email, $company, $facebook);
    }
    /**
     * Check if the desired variable exists in the array
     * @param  [string] $key   [index of the variable in the array]
     * @param  [array] $array
     * @return [array] or NULL       
     */
    public function ifInfoExists($key, $array)
    {
        if(array_has($array, $key)){
            return array_get($array, $key);
        }
        else{
            return NULL;
        }
    }
    /**
     * Checks if socialProfiles key exists
     * @param  array $info 
     * @return array       
     */
    public function ifFullcontactSocialProfileExists($info)
    {
        if (!isset($info['socialProfiles'])) {
            return array('SocialProfiles' => array('url' => 'No social profiles'));
        }
        return $info['socialProfiles'];
    }

    /**
     * Checks if a Facebook link exists in the array
     * @param  array $socialProfiles
     * @return string or NULL
     */
    public function ifFullcontactFbExsists($socialProfiles)
    {
        foreach ($socialProfiles as $socialProfile) {
            foreach ($socialProfile as $index => $url) {
                if($index == 'url'){
                    $string = "/(facebook)/";
                    if (preg_match_all($string, $url)) {
                        return $url;
                    }
                    return NULL;
                }
            }
        }
    }
    
    /**
     * Store to database
     * @param  string $site      
     * @param  string $firstname 
     * @param  string $lastname  
     * @param  string $photo     
     * @param  string $email     
     * @param  string $company   
     * @param  string $facebook  
     * @return void
     */
    public function storeToLocal($site, $firstname, $lastname, $photo, $email, $company, $facebook) {
        DB::insert('insert into contacts (firstname, lastname, photo, email, company, facebook, site) values (?,?,?,?,?,?,?)', [$firstname , $lastname, $photo, $email, $company, $facebook, $site]);
    }
}