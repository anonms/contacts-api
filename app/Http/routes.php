<?php

Route::group(['middleware' => 'web'], function () {
    
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/type', function () {
    return view('type'); // Diri ka una mo access
});
Route::get('/seller/signup', function () {
    return view('find');
});
Route::get('/buyer/registration', function () {
    return view('buyer');
});

Route::post('/seller/signup', 'ScriptController@index'); //Submit email
Route::post('/seller/request', 'ScriptController@request'); //Get data from remote
Route::post('/user/create', 'UsersController@create');

/**
 * Mao ni ang resource Renalyn
 * Example: http://localhost:8000/api/v1/email/{contact}
 */

Route::group(['prefix' => 'api/v1'], function () {
    Route::resource('contact', 'ContactsController');
    Route::get('/email/{contact}', 'ContactsController@show');

});

Route::get('verify/{email}', 'AccessController@verifySeller');
Route::post('verify', 'AccessController@verifySeller');
