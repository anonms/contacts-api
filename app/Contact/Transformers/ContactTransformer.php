<?php

namespace Contact\Transformers;

class ContactTransformer extends Transformer {

	private function transform($contact)
    {
        return [
            'firstname' => $contact['firstname'],
            'lastname' => $contact['lastname'],
            'email' => $contact['email'],
            'company' => $contact['company'],
            'facebook' => $contact['facebook'],
            'avatar' => $contact['photo']
        ];
    }
}