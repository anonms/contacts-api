<?php

namespace Contact\Transformers;

abstract class Tranformer {

	public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
}