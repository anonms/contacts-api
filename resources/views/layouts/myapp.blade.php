<!DOCTYPE html>
<html>
<head>
	<title>App Name - @yield('title')</title>
	@include('layouts.styles')
</head>
<body>
	<div class="container-fluid">
		@yield('content')
	</div>
</body>
	@include('layouts.scripts')
</html>