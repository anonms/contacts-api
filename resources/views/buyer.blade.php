@extends('layouts.myapp')
@section('content')
	<div id="buyer-container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<h2>Buyer Registration</h2>
				<hr>
				<form action="/user/create" method="post">
					<input type="hidden" name="type" value="buyer">
					<div class="form-group">
			          <label class="col-sm-2 control-label">Firstame:</label>
			          <div class="col-sm-9">
			          	<input class="form-control" type="text" name="firstname"required>
			          </div>
			        </div>
					<div class="form-group">
			          <label class="col-sm-2 control-label">Lastname:</label>
			          <div class="col-sm-9">
			          	<input class="form-control" type="text" name="lastname"required>
			          </div>
			        </div>
			        <div class="form-group">
			          <label class="col-sm-2 control-label">Email:</label>
			          <div class="col-sm-9">
			          	<input class="form-control" type="text" name="email"required>
			          </div>
			        </div>
			        <div class="form-group">
			          <label class="col-sm-2 control-label">Password:</label>
			          <div class="col-sm-9">
			          	<input class="form-control" type="password" name="password"required>
			          </div>
			        </div>
			        <div class="form-group">
			        	<div class="col-sm-9 col-sm-offset-2">
			        		<button type="submit" class="btn btn-primary">Confirm</button>
			        		<a href="/type" class="btn btn-danger">Cancel</a>
			        	</div>
			        </div>
				</form>
			</div>
		</div>
	</div>
@endsection