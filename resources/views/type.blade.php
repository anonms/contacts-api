@extends('layouts.myapp')
@section('content')
	<div id="type-container">
		<br><br><br><br><br><br><br><br>
		<div class="row">
			<div id="type-group">
				<h1>Are you a buyer or a seller?</h1>
				<a href="/buyer/registration" class="btn btn-success">Buyer</a>
				<a href="/seller/signup" class="btn btn-primary">Seller</a>	
			</div>
		</div>
	</div>
@endsection