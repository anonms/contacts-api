@extends('layouts.myapp')
@section('content')
	<div class="col-md-4 col-md-offset-4">
		<div id="email-search">
			<h2>Signup using your email</h2>
			<form class="form-group" action="/verify/{$email}" method="post">
				<input class="form-control" type="email" name="email" placeholder="Input email" required>
				<div id="search-button">
					<button class="btn btn-success" type="submit">Submit</button>
					<a href="/type" class="btn btn-danger cancel-button">Cancel</a>
				</div>
			</form>	
		</div>
	</div>
@endsection
