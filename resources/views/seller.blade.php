@extends('layouts.myapp')
@section('content')
	<div id="seller-container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<h2>Seller Registration</h2>
				<hr>
				@if(isset($getClearbit->firstname) || isset($getClearbit->lastname))
					<form action="/user/create" method="post">
						<input type="hidden" name="type" value="seller">
						<div class="form-group">
				          <label class="col-sm-2 control-label">Firstame:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="firstname" value="{{$getClearbit->firstname}}" >
				          </div>
				        </div>
						<div class="form-group">
				          <label class="col-sm-2 control-label">Lastname:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="lastname" value="{{$getClearbit->lastname}}" >
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-2 control-label">Email:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="email" value="{{$getClearbit->email}}" readonly>
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-2 control-label">Password:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="password" name="password" placeholder="Input password" required>
				          </div>
				        </div>
				        <div class="form-group">
				        	<div class="col-sm-9 col-sm-offset-2">
				        		<button type="submit" class="btn btn-primary">Register</button>
				        		<a href="/type" class="btn btn-danger">Cancel</a>
				        	</div>
				        </div>
			        </form>
			    @elseif(isset($getFullcontact->firstname) || isset($getFullcontact->lastname))
			    	<form action="/user/create" method="post">
						<input type="hidden" name="type" value="seller">
						<div class="form-group">
				          <label class="col-sm-2 control-label">Firstame:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="firstname" value="{{$getFullcontact->firstname}}" >
				          </div>
				        </div>
						<div class="form-group">
				          <label class="col-sm-2 control-label">Lastname:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="lastname" value="{{$getFullcontact->lastname}}" >
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-2 control-label">Email:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="text" name="email" value="{{$getFullcontact->email}}" readonly>
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-2 control-label">Password:</label>
				          <div class="col-sm-9">
				          	<input class="form-control" type="password" name="password" placeholder="Input password" required>
				          </div>
				        </div>
				        <div class="form-group">
				        	<div class="col-sm-9 col-sm-offset-2">
				        		<button type="submit" class="btn btn-primary">Register</button>
				        		<a href="/type" class="btn btn-danger">Cancel</a>
				        	</div>
				        </div>
			        </form>
			    @else
		        	<form action="/seller/request" method="post">
		        		<h3>{{$message}}</h3><hr>
		        		<a href="/seller/signup" class="btn btn-success">Return</a>
		        	</form>
				@endif
			</div>
		</div>
	</div>
